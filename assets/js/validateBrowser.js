//run when all assets on website is ready
$(window).on('load', function(){
	//check if ran on inapp browser
	var userAgent = window.navigator.userAgent.toLowerCase(),
		inapp = /instagram|fbav|fban|kakao|snapchat|line|micromessenger|twitter|wechat/.test(userAgent),
		ios = /iphone|ipod|ipad/.test(userAgent),
		safari = /safari/.test(userAgent),
		android = /android/.test(userAgent),
		chrome = /chrome/.test(userAgent);

	//if inapp browser is detected then the user will be prompted
	if(inapp){
		if(android){
			alert("Please open the URL via Chrome for AR experience")
		} else if(ios){
			alert("Please open the URL via Safari for AR experience")
		}
	} else {
		//if it's detected on ios, but not using safari, then we will open safari
		if(ios && !safari) {
			var location = window.location;
			window.open('x-web-search://?ar', "_self");
			window.location.href = location;
		} else if (android) {
			//if it's on android, but not using chrome then we will open chrome
			if(!chrome){
				window.location.href = 'googlechrome://navigate?url='+ location.href;
			}
		}
	}
});